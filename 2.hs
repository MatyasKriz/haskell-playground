module Main where

import Data.List

main :: IO ()

zipWith' :: (a -> b -> c) -> [a] -> [b] -> [c]
zipWith' _ [] _ = []
zipWith' _ _ [] = []
zipWith' f (x:xs) (y:ys) = f x y : zipWith' f xs ys

g = zipWith' (*) [1, 2, 3, 4, 5] [5, 4, 3, 2, 1]

flip' :: (a -> b -> c) -> b -> a -> c
flip' f x y = f y x

map' :: (a -> b) -> [a] -> [b]
map' _ [] = []
map' f (x:xs) = f x : map' f xs

filter' :: (a -> Bool) -> [a] -> [a]
filter' _ [] = []
filter' f (x:xs)
    | f x = x : filter' f xs
    | otherwise = filter' f xs

chain :: (Integral a) => a -> [a]
chain 1 = [1]
chain n
    | even n = n:chain (n `div` 2)
    | odd n = n:chain (n * 3 + 1)

numLongChains :: (Integral a) => a -> Int
numLongChains n = length $ filter ((> 15) . length) $ map chain [1..100]
--numLongChains n = length $ filter (\xs -> length xs > 15) $ map chain [1..100]

elem' :: (Eq a) => a -> [a] -> Bool
elem' y ys = foldl (\acc x -> if x == y then True else acc) False ys

sqrtSums :: Int
sqrtSums = (+ 1) $ length $ takeWhile (< 1000) $ scanl1 (+) $ map sqrt [1..]

oddSquareSum :: Integer
--oddSquareSum = sum . takeWhile (<10000) . filter odd . map (^2) $ [1..]
oddSquareSum =
    let oddSquares = filter odd $ map (^2) [1..]
        belowLimit = takeWhile (< 10000) oddSquares
    in  sum belowLimit

reverseWord :: String -> String -> String
reverseWord curr "" = curr
reverseWord curr (x:"") = x:curr
reverseWord curr (' ':xs) = curr ++ " " ++ reverseWord "" xs
reverseWord curr (x:xs) = reverseWord (x:curr) xs

-- TODO: Make this a one-liner without using other functions.
reverseWords :: String -> String
reverseWords string = reverseWord "" string

modifyNth :: Int -> (a -> a) -> [a] -> [a]
modifyNth _ _ [] = []
modifyNth index f (x:xs)
    | index == 0 = (f x):xs
    | otherwise = x:(modifyNth (index - 1) f xs)

orderedCount :: String -> [(Char, Int)]
-- shorter but N^2
orderedCount s = [(c, length $ filter (==c) s) | c <- nub s]
-- longer but N
--orderedCount = foldl (\list newChar -> let foundIndex = findIndex (\tuple -> fst tuple == newChar) list in maybe (list ++ [(newChar, 1)]) (\index -> modifyNth index (\tuple -> (fst tuple, (snd tuple) + 1)) list) foundIndex) []

main = do
    putStrLn $ show g
    putStrLn $ show (map' (+ 4) [1, 2, 3, 4, 5])
    putStrLn $ show (filter' ((== 0) . flip' mod 2) [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
    putStrLn $ show (numLongChains 100)
    putStrLn $ show (foldl (+) 0 [1, 2, 3, 4, 5])
    putStrLn $ "Do we have it? " ++ show (elem' 13 [1, 13, 15, 16, 13])
    putStrLn $ "Well? " ++ show (sqrtSums)
    putStrLn $ show oddSquareSum
    putStrLn $ reverseWords "Hello there!"
    putStrLn $ show (orderedCount "Abracadabra.")


