module Main where

main :: IO ()

doubleSmallNumber :: Integer -> Integer
doubleSmallNumber x
    | x < 50 = x*2
    | x < 100 = x*3
    | otherwise = x


lostNumbers = 42:[4, 7, 5, 2, 12, 6, 7, 95]

boolToEnglish :: Bool -> String
boolToEnglish True = "Yes."
boolToEnglish _ = "No."

plurality :: Int -> String -> String
plurality count word
    | count == 1 = show count ++ " " ++ word
    | otherwise = show count ++ " " ++ word ++ "s"

enumerate :: [a] -> [(Integer, a)]
enumerate a = zip [1..] a

factorial :: Integral a => a -> a
factorial 0 = 1
factorial n = n * factorial (n - 1)

factorialize :: Integer -> [Integer]
factorialize limit
    | limit < 0 = error "Negative factorial."
    | otherwise = [factorial x | x <- [0..limit]]

euclidean :: [Float] -> [Float] -> Float
euclidean p1 p2
    | (length p1) /= (length p2) = error "Dimensions of passed points must be equal."
    | otherwise = sum [(x1 - x2) ** 2 | (x1, x2) <- zip p1 p2]
p1 = [3, 2]
p2 = [4, 5]

describeList :: [a] -> String
describeList xs = "The list is " ++ case xs of
    [] -> "empty."
    [x] -> "a singleton list."
    xs -> "a longer list."
-- equivalent:
describeList xs = "The list is " ++ what xs
    where what [] = "empty."
          what [x] = "a singleton list."
          what xs = "a longer list."

maximum' :: (Ord a) => [a] -> a  
maximum' [] = error "Maximum of empty list."  
maximum' [x] = x
maximum' (x:xs)
    | x > maxTail = x
    | otherwise = maxTail
    where maxTail = maximum' xs

replicate' :: (Num i, Ord i) => i -> a -> [a]
replicate' count element
    | count <= 0 = []
    | otherwise = element:replicate' (count - 1) element

take' :: (Num i, Ord i) => i -> [a] -> [a]
take' _ [] = []
take' count _ | count <= 0 = []
take' count (x:xs) = x:take' (count - 1) xs

drop' :: (Num i, Ord i) => i -> [a] -> [a]
drop' _ [] = []
drop' count xs | count <= 0 = xs
drop' count (x:xs) = drop' (count - 1) xs

reverse' :: [a] -> [a]
reverse' [] = []
reverse' (x:xs) = (reverse xs) ++ [x]

zip' :: [a] -> [b] -> [(a, b)]
zip' [] _ = []
zip' _ [] = []
zip' (x:xs) (y:ys) = (x, y):zip' xs ys

elem' :: (Eq a) => a -> [a] -> Bool
elem' element [] = False
elem' element (x:xs)
    | element == x = True
    | otherwise = elem' element xs

quicksort :: (Ord a) => [a] -> [a]
quicksort [] = []
quicksort (x:xs) =
    let smallerSorted = quicksort [a | a <- xs, a <= x]
        biggerSorted = quicksort [a | a <- xs, a > x]
    in smallerSorted ++ [x] ++ biggerSorted

main = do
    putStrLn $ show (doubleSmallNumber 30) ++ " " ++ show (doubleSmallNumber 60) ++ " " ++ show (doubleSmallNumber 120)
    putStrLn $ show lostNumbers ++ " @ index 4: " ++ show (lostNumbers !! 4) ++ ", total count: " ++ plurality (length lostNumbers) "element"
    putStrLn $ "Is 10 in the list? " ++ boolToEnglish (10 `elem` lostNumbers)
    putStrLn $ "Enumerated: " ++ show (enumerate lostNumbers)
    putStrLn $ "Factorial of 12: " ++ show (factorialize 12)
    putStrLn $ "Euclidean distance: " ++ show (euclidean p1 p2)
    putStrLn $ "Let pattern: " ++ show ([let square x = x * x in (square 1, square 2, square 3, square 4, square 5)])
    putStrLn $ "Case pattern: " ++ show (describeList lostNumbers)
    putStrLn $ "Max number from list: " ++ show (maximum' lostNumbers)
    putStrLn $ "Replicators: " ++ show (replicate' 3 [1, 2, 3])
    putStrLn $ "Take 6: " ++ show (take' 6 lostNumbers)
    putStrLn $ "Drop 4: " ++ show (drop' 4 lostNumbers)
    putStrLn $ "Zip it, Stan Lee. " ++ show (zip' lostNumbers (reverse' lostNumbers))
    putStrLn $ "Do we have a 42, anyone? " ++ boolToEnglish (42 `elem'` lostNumbers)
    putStrLn $ "Sorta sortin': " ++ show (reverse' $ quicksort lostNumbers)

