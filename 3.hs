module Main where
import Data.List
import Data.Ord

import Numeric

main :: IO ()

descendingOrder :: Integer -> Integer
descendingOrder = foldl (\number digit -> 10 * number + digit) 0 . sortOn Down . map (read . (:[])) . show

seriesSumArg :: Integer -> Integer -> Float
seriesSumArg 0 _ = 0.0
seriesSumArg times divider = (1.0 / (fromInteger divider)) + (seriesSumArg (times - 1) (divider + 3))

seriesSum :: Integer -> String
seriesSum times
    | times == 0 = digits 0.0
    | otherwise = digits (seriesSumArg times 1)
    where digits number = showFFloat (Just 2) number ""

type Ingredient = String
type Amount     = Int
type Recipe     = [(Ingredient, Amount)]
type Storage    = [(Ingredient, Amount)]

cakes :: Recipe -> Storage -> Int
cakes recipe storage
    | foldl (\missingOne ingredient -> if missingOne then missingOne else (snd ingredient) < 0) False (filter (\ingredient -> ingredient `elem` (map fst recipe)) (map fst storage)) = 0
    | otherwise = 1 + (cakes recipe (updateStorage recipe storage))
    where updateStorage recipe storage = map (\ingredient -> let foundRecipeIngredient = find (\ingredient -> (fst ingredient) == (fst ingredient)) recipe in maybe (fst ingredient, snd ingredient) (\recipeIngredient -> (fst ingredient, (snd ingredient) - (snd recipeIngredient)) foundRecipeIngredient)) storage

main = do
    putStrLn $ show (descendingOrder 124443)
    putStrLn $ show (seriesSum 5)
    putStrLn $ show (cakes [("flour",500), ("sugar",200), ("eggs",1)] [("flour",1200), ("sugar",1200), ("eggs",5), ("milk",200)])

